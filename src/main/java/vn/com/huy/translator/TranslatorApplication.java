package vn.com.huy.translator;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vn.com.huy.translator.decorator.DefaultTextDecorator;
import vn.com.huy.translator.decorator.TextDecorator;
import vn.com.huy.translator.provider.PrivateGoogleTranslateAPIProvider;
import vn.com.huy.translator.provider.TranslatorProvider;

public class TranslatorApplication {

    private static final Logger LOG = LoggerFactory.getLogger(TranslatorApplication.class);

    private static final String SUPPORTED_EXTENSION = ".docx";
    private static final String[] BLACK_LIST_CONTAIN = new String[] {
            "ipension.elca-services.com",
    };
    private static final String[] BLACK_LIST_EQUAL = new String[] {
            "-",
            "+",
    };

    private static TranslatorProvider translator = new PrivateGoogleTranslateAPIProvider();
    private static TextDecorator decorator = new DefaultTextDecorator();

    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            LOG.error("Please specify 3 params as below:");
            LOG.error("1st: path to folder that contains document");
            LOG.error("2nd: language of document e.g. fr, de");
            LOG.error("3rd: language that you want to translate to e.g. en, it");
            return;
        }

        // Params:
        // 1st: path to folder
        String path = args[0];
        // 2nd: document language
        String sourceLanguage = args[1];
        // 3rd: translated language
        String targetLanguage = args[2];

        LOG.info("START");
        LOG.info("Using {}", translator.getClass().getSimpleName());
        LOG.info("Using {}", decorator.getClass().getSimpleName());

        try (Stream<Path> stream = Files.list(Paths.get(path))){
            stream
                    .filter(file -> !file.toFile().isHidden())
                    .filter(file -> file.getFileName().toString().endsWith(SUPPORTED_EXTENSION))
                    .forEach(file -> processFile(file, sourceLanguage, targetLanguage));
        }

        LOG.info("DONE");
    }

    private static void processFile(Path path, String srcLang, String targetLang) {
        try {
            String originalFileName = FilenameUtils.getBaseName(path.toFile().getName());
            LOG.info("Translating file: {}", originalFileName);

            String translatedFileName = StringUtils.replace(originalFileName, "_" + srcLang, "_" + targetLang) + SUPPORTED_EXTENSION;

            Path destFolder = Paths.get(path.getParent().toString(), targetLang);
            // Create folder if needed
            destFolder.toFile().mkdirs();

            if (Paths.get(destFolder.toString(), translatedFileName).toFile().exists()) {
                LOG.info("File {} was already translated ({})", originalFileName, translatedFileName);
                return;
            }


            try (InputStream in = Files.newInputStream(path);
                 OutputStream out = Files.newOutputStream(Paths.get(destFolder.toString(), translatedFileName));
                 XWPFDocument doc = new XWPFDocument(in);) {

                processTranslation(doc.getParagraphs(), srcLang, targetLang);
                List<XWPFParagraph> paragraphsInTables = doc.getTables().stream()
                        .flatMap(table -> table.getRows().stream())
                        .flatMap(row -> row.getTableCells().stream())
                        .flatMap(cell -> cell.getParagraphs().stream())
                        .collect(Collectors.toList());
                processTranslation(paragraphsInTables, srcLang, targetLang);

                doc.write(out);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception ex) {
            LOG.error("Error when processing file: " + path, ex);
        }
    }


    private static void processTranslation(List<XWPFParagraph> paragraphs, String sourceLanguage, String targetLanguage) {
        paragraphs.stream()
                .filter(paragraph -> !normalize(paragraph.getText()).isEmpty())
                .filter(paragraph -> !StringUtils.containsAny(normalize(paragraph.getText()), BLACK_LIST_CONTAIN))
                .filter(paragraph -> !StringUtils.equalsAny(normalize(paragraph.getText()), BLACK_LIST_EQUAL))
                .forEach(paragraph -> {
                    if (paragraph.getText().trim().isEmpty()) {
                        return;
                    }

                    String text = paragraph.getText();
                    String translatedText = translator.translate(text, sourceLanguage, targetLanguage);
                    XWPFRun run = paragraph.createRun();
                    run.addBreak(BreakType.TEXT_WRAPPING);
                    decorator.decorate(run, translatedText);

                    LOG.debug("[{}] -> [{}]", text, translatedText);
                });
    }

    private static String normalize(String text) {
        return StringUtils.trimToEmpty(text);
    }

}
