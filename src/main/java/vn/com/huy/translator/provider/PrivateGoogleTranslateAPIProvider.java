package vn.com.huy.translator.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Translator provider that use the private translate API of Google Translate (this API is designed for Chrome plugin) and parse the content of the returned json to get the translated text.
 */
public class PrivateGoogleTranslateAPIProvider implements TranslatorProvider {

    private static final Logger LOG = LoggerFactory.getLogger(PrivateGoogleTranslateAPIProvider.class);

    private boolean useProxy = true;

    public String translate(String text, String langIn, String langOut) {
        try {
            String urlStr = "https://translate.googleapis.com/translate_a/single?" +
                    "client=gtx&" +
                    "sl=" + langIn +
                    "&tl=" + langOut +
                    "&dt=t&q=" + URLEncoder.encode(text, "UTF-8");
            LOG.debug(urlStr);

            URL url = new URL(urlStr);
            URLConnection con = useProxy ? url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.203.2", 8080))) : url.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0");

            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));) {
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                return parseResult(response.toString());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String parseResult(String inputJson) {
        JSONArray jsonArray = new JSONArray(inputJson);
        JSONArray translationsList = (JSONArray) jsonArray.get(0);
        StringBuffer buffer = new StringBuffer();
        for (int i=0; i<translationsList.length(); i++) {
            buffer.append(((JSONArray)translationsList.get(i)).get(0));
        }
        return buffer.toString();
    }

}
