package vn.com.huy.translator.provider;

public interface TranslatorProvider {

    String translate(String input, String inputLanguage, String outputLanguage);

}
